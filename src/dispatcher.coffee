class Dispatcher
  constructor: ->
    @stores = []

  addStore: (store) -> @stores.push store

  dispatch: (action) ->
    store.dispatch action for store in @stores

module.exports = Dispatcher

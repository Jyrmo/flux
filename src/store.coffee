class Store
  constructor: (dispatcher) ->
    @isDispatching = false
    @changed = false
    @listeners = []

  addListener: (onChange) ->
    @listeners.push onChange

  emitChange: ->
    @changed = true

  dispatch: (action) ->
    @isDispatching = true
    @onDispatch action
    @broadcastChange() if @changed
    @changed = false
    @isDispatching = false

  onDispatch: (action) ->

  broadcastChange: ->
    listener() for listener in @listeners

module.exports = Store
